#-*- coding: utf-8 -*-

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

from utils_old import image2gray

def count(data):
    cnt = [0] * 256
    for i in xrange(data.shape[0]):
        for j in xrange(data.shape[1]):
            cnt[int(data[i][j])] += 1
    return cnt
    
def draw(im):
    if isinstance(im, basestring):
        im = Image.open(im)
    im = image2gray(im)
    cnt = count(np.asarray(im))
    plt.bar(range(len(cnt)), cnt, color='gray', label='gray')
    plt.show()
    
if __name__ == '__main__':
    draw('images/test.jpg')