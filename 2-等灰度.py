#-*- coding: utf-8 -*-

from PIL import Image
import numpy as np

def get_data(gray):
    
    def _get_data(i, j):
        k = 3 * gray - i - j
        return [i, j, k] if check(i, j, k) else [255, 255, 255]
    
    return [[_get_data(i, j) for i in range(256)] for j in range(256)]
    
def check(r, g, b):
    return all([0<=it<256 for it in [r, g, b]])
    
if __name__ == '__main__':
    for k in range(256):
        data = np.array(get_data(k), dtype='uint8') # set dtype `uint8` is important
        im = Image.fromarray(data)
        im.save('images_2/%d.bmp' % k)
        print 'NO.%d done..' % k