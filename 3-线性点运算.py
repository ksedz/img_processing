#-*- coding: utf-8 -*-

from PIL import Image
import numpy as np

from utils import handle, gray_bar, get_data

def linear(data, a=1, b=0):
    return a * data + b
    
if __name__ == '__main__':
    filename = 'images/test.jpg'
    im = handle(linear, filename, a=1, b=0)
    im.save('images_3/1.jpg')
    gray_bar(get_data(im))
    
    im = handle(linear, filename, a=1.2, b=0)
    im.save('images_3/2.jpg')
    gray_bar(get_data(im))
    
    im = handle(linear, filename, a=1, b=-20)
    im.save('images_3/3.jpg')
    gray_bar(get_data(im))
    
    im = handle(linear, filename, a=0.5, b=0)
    im.save('images_3/4.jpg')
    gray_bar(get_data(im))