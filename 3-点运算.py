#-*- coding: utf-8 -*-

from PIL import Image
import numpy as np

from utils import limit, gray_bar

def linear(im, a=1, b=0):
    if isinstance(im, basestring):
        im = Image.open(im)
    data = np.array(im, dtype='int')
    data = a * data + b
    return Image.fromarray(limit(data))
    
if __name__ == '__main__':
    file = 'images/test.jpg'
    gray_bar(file)
    im = linear(file, 1.2, 0)
    im.show()
    print np.array(im)
    gray_bar(im)