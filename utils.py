#-*- coding: utf-8 -*-

from sys import argv

from PIL import Image
import matplotlib.pyplot as plt
import numpy as np

def get_data(im, dtype=int):
    if isinstance(im, basestring):
        im = Image.open(im)
    return np.array(im, dtype=dtype)
    
def to_gray(data):
    if len(data.shape) == 3:
        data = np.mean(data, axis=2)
    return data
    
def limit(data, min=0, max=255):
    shape = data.shape
    data = data.reshape(-1)
    for i, num in enumerate(data):
        if num < min:
            data[i] = min
        elif num > max:
            data[i] = max
    return data.reshape(shape) # can use either 'data' or return value
    
def gray_bar(data): # only once
    if len(data.shape) == 3:
        data = to_gray(data)
    cnt = [0] * 256
    for i in xrange(data.shape[0]):
        for j in xrange(data.shape[1]):
            cnt[int(data[i,j])] += 1
    plt.bar(range(len(cnt)), cnt, color='gray', label='gray bar')
    plt.show()
    
def get_im(data):
    data = np.array(limit(data), dtype=np.uint8)
    return Image.fromarray(data)
    
def handle(func, im, *k, **kv):
    return get_im(func(get_data(im), *k, **kv))
    
if __name__ == '__main__':
    for im in argv[1:]:
        data = get_data(im)
        data = to_gray(data)
        im = get_im(data)
        im.show()
        
    t = np.array([[1,2.2],[-1,256]], dtype=float)
    u = limit(t)
    print t
    print u
    
    data = get_data('images/test.jpg')
    data = to_gray(data)
    gray_bar(data)
    
    handle(to_gray, 'images/test.jpg').show()