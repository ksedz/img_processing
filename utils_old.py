#-*- coding: utf-8 -*-

from sys import argv

from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

def image2gray(im):
    if isinstance(im, basestring):
        im = Image.open(im)
    data = np.array(im, dtype='uint8')
    if len(data.shape) == 3:
        data = np.mean(data, axis=2)
    return Image.fromarray(data)
    
def limit(data, min=0, max=255):
    data = np.array(data, dtype=int)
    shape = data.shape
    data = data.reshape(-1)
    for num in data:
        if num < min:
            num = min
        elif num > max:
            num = max
    return np.array(data.reshape(shape), np.uint8) # must use return value because of reshape
        
def gray_bar(im): # only once
    
    def count(data):
        cnt = [0] * 256
        for i in xrange(data.shape[0]):
            for j in xrange(data.shape[1]):
                cnt[int(data[i][j])] += 1
        return cnt

    if isinstance(im, basestring):
        im = Image.open(im)
    im = image2gray(im)
    cnt = count(np.asarray(im))
    plt.bar(range(len(cnt)), cnt, color='gray', label='gray bar')
    plt.show()
        
if __name__ == '__main__':
    for i in argv[1:]:
        im = image2gray(i)
        im.show()
        
    t = np.array([[1,2.2],[-1,256]], dtype=float)
    u = limit(t)
    print t
    print u
    
    gray_bar('images/test.jpg')